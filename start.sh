#!/bin/bash

set -eu

[[ ! -f /app/data/secrets.yaml ]] && cp /app/pkg/secrets.yaml.template /app/data/secrets.yaml

echo "==> Configuring comentario"
yq eval -i ".postgres.host=\"${CLOUDRON_POSTGRESQL_HOST}\"" /app/data/secrets.yaml
yq eval -i ".postgres.port=${CLOUDRON_POSTGRESQL_PORT}" /app/data/secrets.yaml
yq eval -i ".postgres.database=\"${CLOUDRON_POSTGRESQL_DATABASE}\"" /app/data/secrets.yaml
yq eval -i ".postgres.username=\"${CLOUDRON_POSTGRESQL_USERNAME}\"" /app/data/secrets.yaml
yq eval -i ".postgres.password=\"${CLOUDRON_POSTGRESQL_PASSWORD}\"" /app/data/secrets.yaml

yq eval -i ".smtpServer.host=\"${CLOUDRON_MAIL_SMTP_SERVER}\"" /app/data/secrets.yaml
yq eval -i ".smtpServer.port=${CLOUDRON_MAIL_SMTPS_PORT}" /app/data/secrets.yaml
yq eval -i ".smtpServer.username=\"${CLOUDRON_MAIL_SMTP_USERNAME}\"" /app/data/secrets.yaml
yq eval -i ".smtpServer.password=\"${CLOUDRON_MAIL_SMTP_PASSWORD}\"" /app/data/secrets.yaml
yq eval -i ".smtpServer.encryption=\"ssl\"" /app/data/secrets.yaml
yq eval -i ".smtpServer.insecure=true" /app/data/secrets.yaml

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "==> Configuring OIDC auth"
    yq eval -i ".idp.oidc[0].id=\"cloudron\"" /app/data/secrets.yaml
    yq eval -i ".idp.oidc[0].name=\"${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}\"" /app/data/secrets.yaml
    yq eval -i ".idp.oidc[0].url=\"${CLOUDRON_OIDC_ISSUER}\"" /app/data/secrets.yaml
    yq eval -i ".idp.oidc[0].scopes=[\"openid\", \"email\", \"profile\"]" /app/data/secrets.yaml
    yq eval -i ".idp.oidc[0].key=\"${CLOUDRON_OIDC_CLIENT_ID}\"" /app/data/secrets.yaml
    yq eval -i ".idp.oidc[0].secret=\"${CLOUDRON_OIDC_CLIENT_SECRET}\"" /app/data/secrets.yaml
fi

[[ ! -f /app/data/env.sh ]] && cp /app/pkg/env.sh.template /app/data/env.sh

source /app/data/env.sh
export BASE_URL=$CLOUDRON_APP_ORIGIN
export EMAIL_FROM="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Comentario} <${CLOUDRON_MAIL_FROM}>"

chown -R cloudron:cloudron /app/data

echo "==> Starting comentario"
exec gosu cloudron:cloudron ./comentario --secrets=/app/data/secrets.yaml --host=0.0.0.0 --port=8000

