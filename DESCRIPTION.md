## About

Comentario is an open-source web comment engine, which adds discussion functionality to plain, boring web pages.

You can embed it in your website to allow your readers to add comments. It’s fast, powerful, flexible, and easy to use. Have a look at our live demo to see it in action!

## Features in a nutshell

* Privacy by design
* Multilingual
* Role-based access
* Multiple login options
* Hierarchical comments
* Markdown formatting
* Thread locking
* Sticky comments
* Comment editing and deletion
* Comment voting
* Live comment updates
* Custom user avatars
* Email notifications
* Multiple domains in one UI
* Flexible moderation rules
* Extensions
* Statistics
* Data import/export

