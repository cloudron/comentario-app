FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=gitlab-releases depName=comentario/comentario versioning=semver extractVersion=^v(?<version>.+)$ registryUrl=https://gitlab.com
ARG COMENTARIO_VERSION=3.13.0

RUN curl -L https://gitlab.com/api/v4/projects/42486427/packages/generic/comentario/${COMENTARIO_VERSION}/comentario_${COMENTARIO_VERSION}_linux_amd64.tar.gz | tar zxvf - --strip-components 1

COPY env.sh.template secrets.yaml.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

