#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.EMAIL || !process.env.PASSWORD) {
    console.log('USERNAME, EMAIL and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 30000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app, cloudronName;
    const admin_email = 'admin@clodron.local';
    const admin_password = 'Changeme123';
    const admin_name = 'Admin';

    const username = process.env.USERNAME;
    const email = process.env.EMAIL;
    const password = process.env.PASSWORD;

    const domain = 'cloudron.io';
    const blog_name = 'Cloudron blog';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');

        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function signup(email, password, name) {
        await browser.get(`https://${app.fqdn}/en/auth/signup`);
        await waitForElement(By.id('signup-form'));
        await browser.findElement(By.id('email')).sendKeys(email);
        await browser.findElement(By.xpath('//app-password-input[@id="password"]/input')).sendKeys(password);
        await browser.findElement(By.id('name')).sendKeys(name);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[text()="Sign up"]')).click();
        await waitForElement(By.id('login-form'));
    }

    async function login(email, password) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/en/auth/login`);
        await waitForElement(By.id('login-form'));
        await browser.findElement(By.id('email')).sendKeys(email);
        await browser.findElement(By.xpath('//app-password-input[@id="password"]/input')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Sign in"]')).click();
        await waitForElement(By.xpath('//h1[text()="Dashboard"]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/en/auth/login`);

        await browser.sleep(2000);
        await waitForElement(By.id('login-form'));

        await browser.findElement(By.xpath(`//button[contains(., "${cloudronName}")]`)).click();
        await browser.sleep(1000);

        if (!alreadyAuthenticated) {
            const originalWindowHandle = await browser.getWindowHandle();

            await browser.wait(async () => (await browser.getAllWindowHandles()).length === 2, 10000);

            //Loop through until we find a new window handle
            const windows = await browser.getAllWindowHandles();
                windows.forEach(async handle => {
                if (handle !== originalWindowHandle) {
                    await browser.switchTo().window(handle);
                }
            });

            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
            // switch back to the main window
            await browser.switchTo().window(originalWindowHandle);
        }

        await waitForElement(By.xpath('//h1[text()="Dashboard"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/en/manage/dashboard`);
        await waitForElement(By.xpath('//a[@confirmaction="Logout"]'));
        await browser.findElement(By.xpath('//a[@confirmaction="Logout"]')).click();
        await waitForElement(By.xpath('//app-confirm-dialog'));
        await browser.findElement(By.xpath('//button[text()="Logout"]')).click();
        await waitForElement(By.id('login-form'));
    }

    async function addDomain(domain, blog_name) {
        await browser.get(`https://${app.fqdn}/en/manage/domains`);
        await waitForElement(By.xpath('//button[contains(., "New domain")]'));

        await browser.findElement(By.xpath('//button[contains(., "New domain")]')).click();
        await waitForElement(By.id('host'));
        await browser.findElement(By.id('host')).sendKeys(domain);
        await browser.findElement(By.id('name')).sendKeys(blog_name);
        await browser.findElement(By.xpath('//input[@id="host"]/ancestor::form')).submit();
        await waitForElement(By.xpath(`//app-domain-badge/span[text()="${domain}"]`));
    }

    async function checkDomain(domain) {
        await browser.get(`https://${app.fqdn}/en/manage/domains`);
        await waitForElement(By.xpath(`//span[contains(@class, "domain-host") and text()="${domain}"]`));
    }

    async function makeUserSuperAdmin(email) {
        await browser.get(`https://${app.fqdn}/en/manage/users`);
        await waitForElement(By.xpath(`//a[contains(., "${email}")]`));

        await browser.findElement(By.xpath(`//a[contains(., "${email}")]`)).click();

        await waitForElement(By.xpath(`//a[@routerlink="edit"]`));
        await browser.findElement(By.xpath(`//a[@routerlink="edit"]`)).click();

        await waitForElement(By.id('superuser'));

        let superuser = await browser.findElement(By.xpath('//label[@for="superuser"]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', superuser);
        await browser.sleep(2000);
        await superuser.click();

        await browser.findElement(By.xpath('//button[contains(., "Save")]')).click();

        await waitForElement(By.xpath('//div[@class="toast-body" and contains(., "Saved successfully")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can sign up', signup.bind(null, admin_email, admin_password, admin_name));

    it('can login', login.bind(null, admin_email, admin_password));
    it('can add a new domain', addDomain.bind(null, domain, blog_name));
    it('can check domain', checkDomain.bind(null, domain));
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password, false));
    it('can logout', logout);

    it('can login', login.bind(null, admin_email, admin_password));
    it('make OIDC user Superadmin', makeUserSuperAdmin.bind(null, email));
    it('can logout', logout);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id, EXEC_ARGS); });

    it('can login', login.bind(null, admin_email, admin_password));
    it('can check domain', checkDomain.bind(null, domain));
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('can check domain', checkDomain.bind(null, domain));
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login.bind(null, admin_email, admin_password));
    it('can check domain', checkDomain.bind(null, domain));
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('can check domain', checkDomain.bind(null, domain));
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can login', login.bind(null, admin_email, admin_password));
    it('can check domain', checkDomain.bind(null, domain));
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('can check domain', checkDomain.bind(null, domain));
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app for update', function () { execSync('cloudron install --appstore-id app.comentario.cloudronapp --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can sign up', signup.bind(null, admin_email, admin_password, admin_name));

    it('can login', login.bind(null, admin_email, admin_password));
    it('can add a new domain', addDomain.bind(null, domain, blog_name));
    it('can check domain', checkDomain.bind(null, domain));
    it('can logout', logout);

    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('can logout', logout);

    it('can login', login.bind(null, admin_email, admin_password));
    it('can check domain', checkDomain.bind(null, domain));
    it('make OIDC user Superadmin', makeUserSuperAdmin.bind(null, email));
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('can check domain', checkDomain.bind(null, domain));
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
